(example) {

	(mesh) {
		id = "firefly.mesh";
		path = "../data/meshes/firefly.dae";
	}

	(texture) {
		id = "firefly.texture";
		path = "../data/textures/firefly.jpg";
	}

	(shader) {
		id = "firefly.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 vnormals;

			uniform mat4 t_object;
			uniform mat4 t_view;
			uniform mat4 t_projection;

			void main() {
				gl_Position = t_projection * t_view * t_object * vec4(vertices,1);
				vtexcoords = texcoords;
				vnormals = mat3(t_object) * normals;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec3 vnormals;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform sampler2D firefly_tex;

			void main() {
				vec3 n = normalize(vnormals);
				float ambient = 0.2;
				float diffuse = clamp( dot(-lightdir.xyz,n) , ambient, 1);
				fragment = diffuse * texture(firefly_tex,vtexcoords);
			}"
		}
	}

	(matrix) {
		id = "firefly.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "firefly.object";
		mesh = "firefly.mesh";
		shader = "firefly.shader";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "firefly.transform";
		}
	}

	(rendertarget) {
		id = "offline.target";
		width = 800;
		height = 600;
	}

	(texture) {
		id = "offline.texture";
		width = 800;
		height = 600;
		format = "byte";
	}

	(vector) {
		id = "offline.lightdir";
		(value) {"0 1 -1"}
	}

	(matrix) {
		id = "offline.view";
		(value) {
			(view) {
				(position) {"10 10 10"}
				(view) {"-1 -1 -1"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "offline.projection";
		(value) {
			(perspective) {
				angle = 1.5;
				znear = 0.5;
				zfar = 100;
				width = 800;
				height = 600;
			}
		}
	}

	(pass) {
		id = "offline.pass";
		(objects) {
			"firefly.object"
		}
		(vectors) {
			lightdir = "offline.lightdir";
		}
		(matrices) {
			t_view = "offline.view";
			t_projection = "offline.projection";
		}
		(output) {
			rendertarget = "offline.target";
			fragment = "offline.texture";
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(shader) {
		id = "screen.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 fragment;	

			uniform sampler2D offline;

			void main() {
				vec3 val = vec3(0);
				float count = 0;
				for(float px = -0.005 ; px <= 0.005 ; px = px + 0.001) {
					for(float py = -0.005 ; py <= 0.005 ; py = py + 0.001) {
						val += texture(offline,vtexcoords + vec2(px,py)).rgb;
						count++;
					}
				}
				fragment = vec4(val / count,1);
			}"
		}
	}

	(pass) {
		id = "screen.pass";
		shader = "screen.shader";
		backgroundQuad = "true";
		(textures) {
			offline = "offline.texture";
		}
		(output) {
			rendertarget = "screen.target";
		}
	}

	(effect) {
		id = "effect";
		(passes) {
			"offline.pass"
			"screen.pass"
		}
	}

}
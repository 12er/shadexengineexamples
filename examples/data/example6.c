(example) {

	//render objects

	(mesh) {
		id = "firefly.mesh";
		path = "../data/meshes/firefly.dae";
	}

	(texture) {
		id = "firefly.texture";
		path = "../data/textures/firefly.jpg";
	}

	(matrix) {
		id = "firefly.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "firefly.object";
		mesh = "firefly.mesh";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "firefly.transform";
		}
	}

	(mesh) {
		id = "quad.mesh";
		faceSize = 3;
		(attribute) {
			name = "vertices";
			attributeSize = 3;
			(data) {
				"-10 -10 -5
				10 -10 -5
				10 10 -5
				-10 -10 -5
				10 10 -5
				-10 10 -5"
			}
		}
		(attribute) {
			name = "normals";
			attributeSize = 3;
			(data) {
				"0 0 1
				0 0 1
				0 0 1
				0 0 1
				0 0 1
				0 0 1"
			}
		}
		(attribute) {
			name = "texcoords";
			attributeSize = 2;
			(data) {
				"0 0
				1 0
				1 1
				0 0
				1 1
				0 1"
			}
		}
	}

	(matrix) {
		id = "quad.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "quad.object";
		mesh = "quad.mesh";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "quad.transform";
		}
	}

	//resources of the shadowmapping pass

	(shader) {
		id = "shadowmap.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out float vdepth;
			
			uniform mat4 t_object;
			uniform mat4 t_light;

			void main() {
				vec4 pos = t_light * t_object * vec4(vertices,1);
				gl_Position = pos;
				vdepth = pos.z;
			}"
		}
		(fragment) {"
			#version 400 core
			in float vdepth;
			out vec4 shadowmap;
			
			void main() {
				float shadow_offset = 0.01;
				shadowmap = vec4(vdepth + shadow_offset,0,0,1);
			}"
		}
	}

	(rendertarget) {
		id = "shadowmap.target";
		width = 500;
		height = 500;
	}

	(texture) {
		id = "shadowmap.texture";
		width = 500;
		height = 500;
		format = "float16";
	}

	(matrix) {
		id = "shadowmap.transform";
		(value) {
			(orthographic) {
				left = -15;
				right = 15;
				bottom = -15;
				top = 15;
				znear = 0.1;
				zfar = 20;
			}
			(view) {
				(position) {"0 0 10"}
				(view) {"0 0 -1"}
				(up) {"1 0 0"}
			}
		}
	}

	(matrix) {
		id = "shadowmap.light2texture";
		(value) {
			(translate) {"0.5 0.5 0"}
			(scale) {"0.5 0.5 1"}
		}
	}

	(pass) {
		id = "shadowmap.pass";
		shader = "shadowmap.shader";
		(matrices) {
			t_light = "shadowmap.transform";
		}
		(objects) {
			"firefly.object"
			"quad.object"
		}
		(output) {
			rendertarget = "shadowmap.target";
			shadowmap = "shadowmap.texture";
		}
	}

	//resources of the screen pass

	(vector) {
		id = "screen.lightdir";
		(value) {"0 0 -1"}
	}

	(matrix) {
		id = "screen.view";
		(value) {
			(view) {
				(position) {"10 10 10"}
				(view) {"-1 -1 -1"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "screen.projection";
		(value) {
			(perspective) {
				angle = 1.5;
				znear = 0.5;
				zfar = 100;
				width = 800;
				height = 600;
			}
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(shader) {
		id = "screen.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 vnormals;
			out vec4 fragDepth;

			uniform mat4 t_object;
			uniform mat4 t_view;
			uniform mat4 t_projection;
			uniform mat4 t_light;
			uniform mat4 t_tex;

			void main() {
				gl_Position = t_projection * t_view * t_object * vec4(vertices,1);
				fragDepth = t_tex * t_light * t_object * vec4(vertices,1);

				vtexcoords = texcoords;
				vnormals = mat3(t_object) * normals;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec3 vnormals;
			in vec4 fragDepth;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform sampler2D firefly_tex;
			uniform sampler2D shadowmap;

			void main() {
				float ambient = 0.2;

				vec3 fd = fragDepth.xyz / fragDepth.w;
				float frag_depth = fd.z;
				float map_depth = texture(shadowmap,fd.xy).r;
				float shadow = 1.0;
				if(frag_depth > map_depth) {
					shadow = ambient;
				}

				vec3 n = normalize(vnormals);
				float diffuse = clamp( dot(-lightdir.xyz,n) , ambient, 1);

				float factor = min(diffuse,shadow);
				fragment = factor * texture(firefly_tex,vtexcoords);
			}"
		}
	}

	(pass) {
		id = "screen.pass";
		shader = "screen.shader";
		(vectors) {
			lightdir = "screen.lightdir";
		}
		(matrices) {
			t_light = "shadowmap.transform";
			t_tex = "shadowmap.light2texture";
			t_view = "screen.view";
			t_projection = "screen.projection";
		}
		(textures) {
			shadowmap = "shadowmap.texture";
		}
		(objects) {
			"firefly.object"
			"quad.object"
		}
		(output) {
			rendertarget = "screen.target";
		}
	}

	(effect) {
		id = "effect";
		(passes) {
			"shadowmap.pass"
			"screen.pass"
		}
	}

}
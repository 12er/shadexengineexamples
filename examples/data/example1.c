(example) {

	(shader) {
		id = "screen.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 fragment;

			void main() {
				fragment = vec4(0,vtexcoords,1);
			}"
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(pass) {
		id = "screen.pass";
		shader = "screen.shader";
		backgroundQuad = "true";
		(output) {
			rendertarget = "screen.target";
		}
	}

}
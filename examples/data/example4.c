(example) {

	(mesh) {
		id = "firefly.mesh";
		path = "../data/meshes/firefly.dae";
	}

	(texture) {
		id = "firefly.texture";
		path = "../data/textures/firefly.jpg";
	}

	(shader) {
		id = "firefly.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 vnormals;

			uniform mat4 t_object;
			uniform mat4 t_view;
			uniform mat4 t_projection;

			void main() {
				gl_Position = t_projection * t_view * t_object * vec4(vertices,1);
				vtexcoords = texcoords;
				vnormals = mat3(t_object) * normals;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec3 vnormals;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform sampler2D firefly_tex;

			void main() {
				vec3 n = normalize(vnormals);
				float ambient = 0.2;
				float diffuse = clamp( dot(-lightdir.xyz,n) , ambient, 1);
				fragment = diffuse * texture(firefly_tex,vtexcoords);
			}"
		}
	}

	(matrix) {
		id = "firefly.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "firefly.object";
		mesh = "firefly.mesh";
		shader = "firefly.shader";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "firefly.transform";
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(vector) {
		id = "screen.lightdir";
		(value) {"0 1 -1"}
	}

	(matrix) {
		id = "screen.view";
		(value) {
			(view) {
				(position) {"10 10 10"}
				(view) {"-1 -1 -1"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "screen.projection";
		(value) {
			(perspective) {
				angle = 1.5;
				znear = 0.5;
				zfar = 100;
				width = 800;
				height = 600;
			}
		}
	}

	(pass) {
		id = "screen.pass";
		(objects) {
			"firefly.object"
		}
		(vectors) {
			lightdir = "screen.lightdir";
		}
		(matrices) {
			t_view = "screen.view";
			t_projection = "screen.projection";
		}
		(output) {
			rendertarget = "screen.target";
		}
	}

}
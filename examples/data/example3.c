(example) {

	(mesh) {
		id = "triangle.mesh";
		faceSize = 3;
		
		(attribute) {
			name = "vertices";
			attributeSize = 2;
			(data) {
				"-0.5 -0.5
				0.5 -0.5
				0 0.5"
			}
		}

		(attribute) {
			name = "colors";
			attributeSize = 3;
			(data) {
				"0 1 0
				0 0 1
				0 1 1"
			}
		}

	}

	(shader) {
		id = "triangle.shader";
		(vertex) {"
			#version 400 core
			in vec2 vertices;
			in vec3 colors;
			out vec3 vcolors;

			uniform vec4 objectdata;
			uniform mat4 passtransform;

			void main() {
				gl_Position = passtransform * vec4(vertices,0,1) + vec4(objectdata.xyz,0);
				vcolors = colors;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec3 vcolors;
			out vec4 fragment;

			void main() {
				fragment = vec4(vcolors,1);
			}"
		}
	}

	(object) {
		id = "triangle.object";
		mesh = "triangle.mesh";
		shader = "triangle.shader";
		(vectors) {
			objectdata = "triangle.translation";
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(pass) {
		id = "screen.pass";
		(objects) {
			"triangle.object"
		}
		(matrices) {
			passtransform = "pass.rotation";
		}
		(output) {
			rendertarget = "screen.target";
		}
	}

}
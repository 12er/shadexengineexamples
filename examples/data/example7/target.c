(target) {

	//shadow settings

	(matrix) {
		id = "shadowmap.transform";
		(value) {
			(orthographic) {
				left = -15;
				right = 15;
				bottom = -15;
				top = 15;
				znear = 0.1;
				zfar = 20;
			}
			(view) {
				(position) {"0 0 10"}
				(view) {"0 0 -1"}
				(up) {"1 0 0"}
			}
		}
	}

	(matrix) {
		id = "shadowmap.light2texture";
		(value) {
			(translate) {"0.5 0.5 0"}
			(scale) {"0.5 0.5 1"}
		}
	}

	//camera settings

	(vector) {
		id = "screen.lightdir";
		(value) {"0 0 -1"}
	}

	(matrix) {
		id = "screen.view";
		(value) {
			(view) {
				(position) {"10 10 10"}
				(view) {"-1 -1 -1"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "screen.projection";
		(value) {
			(perspective) {
				angle = 1.5;
				znear = 0.5;
				zfar = 100;
				width = 800;
				height = 600;
			}
		}
	}

	(rendertarget) {
		id = "screen.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

}
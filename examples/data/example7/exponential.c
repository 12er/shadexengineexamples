(exponential) {

	(shader) {
		id = "shadowmap.exponential.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			out float vdepth;
			
			uniform mat4 t_object;
			uniform mat4 t_light;

			void main() {
				vec4 pos = t_light * t_object * vec4(vertices,1);
				gl_Position = pos;
				vdepth = pos.z;
			}"
		}
		(fragment) {"
			#version 400 core
			in float vdepth;
			out vec4 shadowmap;
			
			void main() {
				shadowmap = vec4(vdepth,0,0,1);
			}"
		}
	}

	(pass) {
		id = "shadowmap.exponential.pass";
		shader = "shadowmap.exponential.shader";
		(matrices) {
			t_light = "shadowmap.transform";
		}
		(output) {
			rendertarget = "shadowmap.target";
			shadowmap = "unfiltered.texture";
		}
	}

	//for filtering see gaussianfilter.c

	//resources of the screen pass

	(shader) {
		id = "screen.exponential.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 vnormals;
			out vec4 fragDepth;

			uniform mat4 t_object;
			uniform mat4 t_view;
			uniform mat4 t_projection;
			uniform mat4 t_light;
			uniform mat4 t_tex;

			void main() {
				gl_Position = t_projection * t_view * t_object * vec4(vertices,1);
				fragDepth = t_tex * t_light * t_object * vec4(vertices,1);

				vtexcoords = texcoords;
				vnormals = mat3(t_object) * normals;
			}"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec3 vnormals;
			in vec4 fragDepth;
			out vec4 fragment;

			uniform vec4 lightdir;
			uniform sampler2D firefly_tex;
			uniform sampler2D shadowmap;

			void main() {
				float ambient = 0.2;

				vec3 fd = fragDepth.xyz / fragDepth.w;
				float t = fd.z;
				float x = texture(shadowmap,fd.xy).r;
				float shadow = clamp(exp(x-t)*2.5 - 1.5,ambient,1);


				vec3 n = normalize(vnormals);
				float diffuse = clamp( dot(-lightdir.xyz,n) , ambient, 1);

				float factor = min(diffuse,shadow);
				fragment = factor * texture(firefly_tex,vtexcoords);
			}"
		}
	}

	(pass) {
		id = "screen.exponential.pass";
		shader = "screen.exponential.shader";
		(vectors) {
			lightdir = "screen.lightdir";
		}
		(matrices) {
			t_light = "shadowmap.transform";
			t_tex = "shadowmap.light2texture";
			t_view = "screen.view";
			t_projection = "screen.projection";
		}
		(textures) {
			shadowmap = "shadowmap.texture";
		}
		(output) {
			rendertarget = "screen.target";
		}
	}

	(effect) {
		id = "effect.exponential";
		(passes) {
			"shadowmap.exponential.pass"
			"filter1.pass"
			"filter2.pass"
			"screen.exponential.pass"
		}
	}

}
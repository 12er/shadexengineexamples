(gaussianfilter) {

	(rendertarget) {
		id = "shadowmap.target";
		width = 500;
		height = 500;
	}

	(texture) {
		id = "shadowmap.texture";
		width = 500;
		height = 500;
		format = "float16";
	}

	(texture) {
		id = "unfiltered.texture";
		width = 500;
		height = 500;
		format = "float16";
	}

	(texture) {
		id = "halffiltered.texture";
		width = 500;
		height = 500;
		format = "float16";
	}

	(float) {
		id = "kernelsize";
		value = 0.01;
	}

	//filter shadowmap - first pass

	(shader) {
		id = "filter1.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords[15];

			uniform float kernelsize;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords[0] = texcoords + vec2(0,-1)*kernelsize;
				vtexcoords[1] = texcoords + vec2(0,-0.85714285)*kernelsize;
				vtexcoords[2] = texcoords + vec2(0,-0.71428571)*kernelsize;
				vtexcoords[3] = texcoords + vec2(0,-0.57142857)*kernelsize;
				vtexcoords[4] = texcoords + vec2(0,-0.42857142)*kernelsize;
				vtexcoords[5] = texcoords + vec2(0,-0.28571428)*kernelsize;
				vtexcoords[6] = texcoords + vec2(0,-0.14285714)*kernelsize;
				vtexcoords[7] = texcoords;
				vtexcoords[8] = texcoords + vec2(0,0.14285714)*kernelsize;
				vtexcoords[9] = texcoords + vec2(0,0.28571428)*kernelsize;
				vtexcoords[10] = texcoords + vec2(0,0.42857142)*kernelsize;
				vtexcoords[11] = texcoords + vec2(0,0.57142857)*kernelsize;
				vtexcoords[12] = texcoords + vec2(0,0.71428571)*kernelsize;
				vtexcoords[13] = texcoords + vec2(0,0.85714285)*kernelsize;
				vtexcoords[14] = texcoords + vec2(0,1)*kernelsize;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords[15];
			out vec4 filtered;

			uniform sampler2D unfiltered;

			void main() {
				filtered += texture(unfiltered,vtexcoords[0])*0.00442991210551132;
				filtered += texture(unfiltered,vtexcoords[1])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[2])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[3])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[4])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[5])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[6])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[7])*0.159576912161;
				filtered += texture(unfiltered,vtexcoords[8])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[9])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[10])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[11])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[12])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[13])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[14])*0.00442991210551132;
			}
			"
		}
	}

	(pass) {
		id = "filter1.pass";
		shader = "filter1.shader";
		backgroundQuad = "true";
		(textures) {
			unfiltered = "unfiltered.texture";
		}
		(floats) {
			kernelsize= "kernelsize";
		}
		(output) {
			rendertarget = "shadowmap.target";
			filtered = "halffiltered.texture";
		}
	}

	//filter shadowmap - second pass

	(shader) {
		id = "filter2.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords[15];

			uniform float kernelsize;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords[0] = texcoords + vec2(-1,0)*kernelsize;
				vtexcoords[1] = texcoords + vec2(-0.85714285,0)*kernelsize;
				vtexcoords[2] = texcoords + vec2(-0.71428571,0)*kernelsize;
				vtexcoords[3] = texcoords + vec2(-0.57142857,0)*kernelsize;
				vtexcoords[4] = texcoords + vec2(-0.42857142,0)*kernelsize;
				vtexcoords[5] = texcoords + vec2(-0.28571428,0)*kernelsize;
				vtexcoords[6] = texcoords + vec2(-0.14285714,0)*kernelsize;
				vtexcoords[7] = texcoords;
				vtexcoords[8] = texcoords + vec2(0.14285714,0)*kernelsize;
				vtexcoords[9] = texcoords + vec2(0.28571428,0)*kernelsize;
				vtexcoords[10] = texcoords + vec2(0.42857142,0)*kernelsize;
				vtexcoords[11] = texcoords + vec2(0.57142857,0)*kernelsize;
				vtexcoords[12] = texcoords + vec2(0.71428571,0)*kernelsize;
				vtexcoords[13] = texcoords + vec2(0.85714285,0)*kernelsize;
				vtexcoords[14] = texcoords + vec2(1,0)*kernelsize;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords[15];
			out vec4 filtered;

			uniform sampler2D unfiltered;

			void main() {
				filtered += texture(unfiltered,vtexcoords[0])*0.00442991210551132;
				filtered += texture(unfiltered,vtexcoords[1])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[2])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[3])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[4])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[5])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[6])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[7])*0.159576912161;
				filtered += texture(unfiltered,vtexcoords[8])*0.147308056121;
				filtered += texture(unfiltered,vtexcoords[9])*0.115876621105;
				filtered += texture(unfiltered,vtexcoords[10])*0.0776744219933;
				filtered += texture(unfiltered,vtexcoords[11])*0.0443683338718;
				filtered += texture(unfiltered,vtexcoords[12])*0.0215963866053;
				filtered += texture(unfiltered,vtexcoords[13])*0.00895781211794;
				filtered += texture(unfiltered,vtexcoords[14])*0.00442991210551132;
			}
			"
		}
	}

	(pass) {
		id = "filter2.pass";
		shader = "filter2.shader";
		backgroundQuad = "true";
		(textures) {
			unfiltered = "halffiltered.texture";
		}
		(floats) {
			kernelsize= "kernelsize";
		}
		(output) {
			rendertarget = "shadowmap.target";
			filtered = "shadowmap.texture";
		}
	}

}
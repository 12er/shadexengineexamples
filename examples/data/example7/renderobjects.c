(renderobjects) {	

	(mesh) {
		id = "firefly.mesh";
		path = "../data/meshes/firefly.dae";
	}

	(texture) {
		id = "firefly.texture";
		path = "../data/textures/firefly.jpg";
	}

	(matrix) {
		id = "firefly.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "firefly.object";
		mesh = "firefly.mesh";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "firefly.transform";
		}
	}

	(mesh) {
		id = "quad.mesh";
		faceSize = 3;
		(attribute) {
			name = "vertices";
			attributeSize = 3;
			(data) {
				"-10 -10 -5
				10 -10 -5
				10 10 -5
				-10 -10 -5
				10 10 -5
				-10 10 -5"
			}
		}
		(attribute) {
			name = "normals";
			attributeSize = 3;
			(data) {
				"0 0 1
				0 0 1
				0 0 1
				0 0 1
				0 0 1
				0 0 1"
			}
		}
		(attribute) {
			name = "texcoords";
			attributeSize = 2;
			(data) {
				"0 0
				1 0
				1 1
				0 0
				1 1
				0 1"
			}
		}
	}

	(matrix) {
		id = "quad.transform";
		(value) {
			"1 0 0 0
			0 1 0 0
			0 0 1 0
			0 0 0 1"
		}
	}

	(object) {
		id = "quad.object";
		mesh = "quad.mesh";
		(textures) {
			firefly_tex = "firefly.texture";
		}
		(matrices) {
			t_object = "quad.transform";
		}
	}

	//adding the renderobjects to the passes

	(pass) {
		id = "shadowmap.simple.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

	(pass) {
		id = "screen.simple.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

	(pass) {
		id = "shadowmap.variance.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

	(pass) {
		id = "screen.variance.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

	(pass) {
		id = "shadowmap.exponential.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

	(pass) {
		id = "screen.exponential.pass";
		(objects) {
			"firefly.object"
			"quad.object"
		}
	}

}
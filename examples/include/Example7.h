#ifndef _EXAMPLE_H_
#define _EXAMPLE_H_

#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <QWidget>
#include <QLineEdit>
using namespace sx;

enum AlgorithmNumber {
	ALGORITHMVARIANCE = 0,
	ALGORITHMEXPONENTIAL = 1,
	ALGORITHMSIMPLE = 2
};

class ShadowmapSizePanel: public QWidget {

	Q_OBJECT

private:
	int width;
	int height;
	float kernelsize;
	QLineEdit *textWidth;
	QLineEdit *textHeight;
	QLineEdit *textKernel;

	ShadowmapSizePanel(const ShadowmapSizePanel &);
	ShadowmapSizePanel &operator = (const ShadowmapSizePanel &);
private slots:

	void readInput();

signals:

	void emitSize(int width, int height, float kernelsize);

public:
	ShadowmapSizePanel();
};

class ExampleListener: public QObject, public SXRenderListener {

	Q_OBJECT

private:

	ShadeX shadeX;
	AlgorithmNumber smAlgorithm;

	ExampleListener(const ExampleListener &);
	ExampleListener &operator = (const ExampleListener &);
public slots:

	void chooseAlgorithm(int algorithmNum);
	void setShadowmapSize(int width, int height, float kernelsize);

public:
	ExampleListener();

	void create(SXRenderArea &area);
	void reshape(SXRenderArea &area);
	void render(SXRenderArea &area);
	void stop(SXRenderArea &area);
};

#endif
#ifndef _EXAMPLE_H_
#define _EXAMPLE_H_

#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
using namespace sx;

class ExampleListener: public SXRenderListener {
	ShadeX shadeX;

	ExampleListener(const ExampleListener &);
	ExampleListener &operator = (const ExampleListener &);
public:
	ExampleListener();

	void create(SXRenderArea &area);
	void reshape(SXRenderArea &area);
	void render(SXRenderArea &area);
	void stop(SXRenderArea &area);
};

#endif
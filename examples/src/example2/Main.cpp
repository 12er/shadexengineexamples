#ifndef _MAIN_CPP_
#define _MAIN_CPP_

#include <Example2.h>
#include <QApplication>
#include <sx/SXWidget.h>
using namespace sx;

int main(int argc, char **argv) {
	try {
		Logger::addLogger("examplelogger",new FileLogger("Example2.html"));
		Logger::setDefaultLogger("examplelogger");
	} catch(Exception &e) {
		Logger::get() << Level(L_FATAL_ERROR) << e.getMessage();
		return 1;
	}
	
	QApplication app(argc,argv);

	SXWidget widget;
	widget.setMinimumSize(800,600);
	QObject::connect(&widget,SIGNAL(finishedRendering()),&widget,SLOT(close()));
	widget.renderPeriodically(0.01f);
	widget.addRenderListener(*new ExampleListener());
	widget.show();

	return app.exec();
}

#endif
#ifndef _EXAMPLE_EXAMPLE_CPP_
#define _EXAMPLE_EXAMPLE_CPP_

#include <Example2.h>

ExampleListener::ExampleListener() {
}

void ExampleListener::create(SXRenderArea &area) {
	try {
		shadeX.addResources("../data/example2.c");
		shadeX.load();
	} catch(Exception &e) {
		Logger::get() << Level(L_ERROR) << e.getMessage();
		area.stopRendering();
		return;
	}
}

void ExampleListener::reshape(SXRenderArea &area) {
	RenderTarget &screen = shadeX.getRenderTarget("screen.target");
	screen.setWidth(area.getWidth());
	screen.setHeight(area.getHeight());
	screen.load();
}

void ExampleListener::render(SXRenderArea &area) {
	Pass &pass = shadeX.getPass("screen.pass");
	pass.render();
}

void ExampleListener::stop(SXRenderArea &area) {
}

#endif
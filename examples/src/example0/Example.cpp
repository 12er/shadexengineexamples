#ifndef _EXAMPLE_EXAMPLE_CPP_
#define _EXAMPLE_EXAMPLE_CPP_

#include <Example0.h>

ExampleListener::ExampleListener() {
}

void ExampleListener::create(SXRenderArea &area) {
	Logger::get() << LogMarkup("RenderListener log output");
	Logger::get() << Annotation("create") << "here resources for the renderer could be constructed";
}

void ExampleListener::reshape(SXRenderArea &area) {
	Logger::get() << Annotation("reshape") << "reshape, the widget has dimensions (" << area.getWidth() << " , " << area.getHeight() << ")";
}

void ExampleListener::render(SXRenderArea &area) {
	if(area.getTime() > 5) {
		area.stopRendering();
	}
	Logger::get() << Annotation("render") << "here could be some code for rendering, the widget has dimensions (" << area.getWidth() << " , " << area.getHeight() << ")";
}

void ExampleListener::stop(SXRenderArea &area) {
	Logger::get() << Annotation("stop") << "here resources could be deconstructed";
}

#endif
#ifndef _EXAMPLE_SHADOWMAPSIZEPANEL_CPP_
#define _EXAMPLE_SHADOWMAPSIZEPANEL_CPP_

#include <Example7.h>
#include <QGridLayout>
#include <QLineEdit>
#include <QApplication>
#include <QFontMetrics>
#include <QPushButton>
#include <QLabel>

void ShadowmapSizePanel::readInput() {
	bool syntaxWidth;
	bool syntaxHeight;
	bool syntaxKernel;
	int tempWidth = textWidth->text().toInt(&syntaxWidth);
	int tempHeight = textHeight->text().toInt(&syntaxHeight);
	float tempKernel = textKernel->text().toFloat(&syntaxKernel);
	if(tempWidth > 0 && tempHeight > 0 && syntaxWidth && syntaxHeight && syntaxKernel) {
		width = tempWidth;
		height = tempHeight;
		kernelsize = tempKernel;
		emitSize(width,height,kernelsize);
	} else {
		textWidth->setText(QString::number(width));
		textHeight->setText(QString::number(height));
		textKernel->setText(QString::number(kernelsize));
	}
}

ShadowmapSizePanel::ShadowmapSizePanel(): QWidget() {
	width = 500;
	height = 500;
	kernelsize = 0.01f;

	QGridLayout *layout = new QGridLayout();
	setLayout(layout);

	QFontMetrics metrics(QApplication::font());

	QLabel *labelWidth = new QLabel("width");
	labelWidth->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	layout->addWidget(labelWidth,1,1);

	textWidth = new QLineEdit("500");
	textWidth->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	textWidth->setFixedWidth(metrics.width("WWWWW"));
	layout->addWidget(textWidth,1,2);

	QLabel *labelHeight = new QLabel("height");
	labelHeight->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	layout->addWidget(labelHeight,2,1);

	textHeight = new QLineEdit("500");
	textHeight->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	textHeight->setFixedWidth(metrics.width("WWWWW"));
	layout->addWidget(textHeight,2,2);

	QLabel *labelKernel = new QLabel("kernel size");
	labelKernel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	layout->addWidget(labelKernel,3,1);

	textKernel = new QLineEdit("0.01");
	textKernel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	textKernel->setFixedWidth(metrics.width("WWWWW"));
	layout->addWidget(textKernel,3,2);

	QPushButton *button = new QPushButton("update shadowmap settings");
	button->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	layout->addWidget(button,4,1,1,2);

	QObject::connect(button,SIGNAL(clicked()),this,SLOT(readInput()));
}

#endif
#ifndef _EXAMPLE_EXAMPLE_CPP_
#define _EXAMPLE_EXAMPLE_CPP_

#include <Example7.h>

void ExampleListener::chooseAlgorithm(int algorithmNum) {
	smAlgorithm = (AlgorithmNumber)algorithmNum;
}

void ExampleListener::setShadowmapSize(int width, int height, float kernelsize) {
	RenderTarget &target = shadeX.getRenderTarget("shadowmap.target");
	Texture &shadowmap = shadeX.getTexture("shadowmap.texture");
	Texture &unfiltered = shadeX.getTexture("unfiltered.texture");
	Texture &halffiltered = shadeX.getTexture("halffiltered.texture");
	UniformFloat &uniformKernel = shadeX.getUniformFloat("kernelsize");
	target.setWidth(width);
	target.setHeight(height);
	target.setRenderToDisplay(false);
	target.load();
	shadowmap.setWidth(width);
	shadowmap.setHeight(height);
	shadowmap.setPixelFormat(FLOAT16_RGBA);
	shadowmap.load();
	unfiltered.setWidth(width);
	unfiltered.setHeight(height);
	unfiltered.setPixelFormat(FLOAT16_RGBA);
	unfiltered.load();
	halffiltered.setWidth(width);
	halffiltered.setHeight(height);
	halffiltered.setPixelFormat(FLOAT16_RGBA);
	halffiltered.load();
	uniformKernel.value = kernelsize;
}

ExampleListener::ExampleListener(): QObject() {
	smAlgorithm = ALGORITHMVARIANCE;
}

void ExampleListener::create(SXRenderArea &area) {
	try {
		shadeX.addResources("../data/example7/renderobjects.c");
		shadeX.addResources("../data/example7/target.c");
		shadeX.addResources("../data/example7/gaussianfilter.c");
		shadeX.addResources("../data/example7/variance.c");
		shadeX.addResources("../data/example7/exponential.c");
		shadeX.addResources("../data/example7/simple.c");
		shadeX.load();
	} catch(Exception &e) {
		Logger::get() << Level(L_ERROR) << e.getMessage();
		area.stopRendering();
		return;
	}
}

void ExampleListener::reshape(SXRenderArea &area) {
	RenderTarget &screen = shadeX.getRenderTarget("screen.target");
	screen.setWidth(area.getWidth());
	screen.setHeight(area.getHeight());
	screen.load();

	Matrix &projection = shadeX.getUniformMatrix("screen.projection");
	projection.perspectiveMatrix((float)Pi*0.5f, (float)area.getWidth(), (float)area.getHeight(), 0.1f, 100.0f);
}

void ExampleListener::render(SXRenderArea &area) {
	if(smAlgorithm == ALGORITHMVARIANCE) {
		Effect &effect = shadeX.getEffect("effect.variance");
		effect.render();
	} else if(smAlgorithm == ALGORITHMEXPONENTIAL) {
		Effect &effect = shadeX.getEffect("effect.exponential");
		effect.render();
	} else if(smAlgorithm == ALGORITHMSIMPLE) {
		Effect &effect = shadeX.getEffect("effect.simple");
		effect.render();
	}
	
	float time = (float)area.getTime();
	Matrix &fireflyTransform = shadeX.getUniformMatrix("firefly.transform");
	fireflyTransform = Matrix().rotate(Vector(0,0,1),(float)Pi * 0.2f * time) * Matrix().rotate(Vector(0,1,0),(float)Pi * time);
}

void ExampleListener::stop(SXRenderArea &area) {
}

#endif
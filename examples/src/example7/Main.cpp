#ifndef _MAIN_CPP_
#define _MAIN_CPP_

#include <Example7.h>
#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <string>
#include <sx/SXWidget.h>
using namespace sx;
using namespace std;

const string algorithmVariance = "variance shadowmapping";
const string algorithmExponential = "exponential shadowmapping";
const string algorithmSimple = "simple shadowmapping";

int main(int argc, char **argv) {
	try {
		Logger::addLogger("examplelogger",new FileLogger("Example7.html"));
		Logger::setDefaultLogger("examplelogger");
	} catch(Exception &e) {
		Logger::get() << Level(L_FATAL_ERROR) << e.getMessage();
		return 1;
	}
	
	QApplication app(argc,argv);

	QWidget widget;
	QGridLayout *layout = new QGridLayout();
	widget.setLayout(layout);

	ExampleListener *exampleListener = new ExampleListener();
	SXWidget *renderer = new SXWidget();
	renderer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	renderer->setMinimumSize(800,600);
	QObject::connect(renderer,SIGNAL(finishedRendering()),renderer,SLOT(close()));
	renderer->renderPeriodically(0.01f);
	renderer->addRenderListener(*exampleListener);
	layout->addWidget(renderer,1,1);

	QWidget *controls = new QWidget();
	controls->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	QGridLayout *controlLayout = new QGridLayout();
	controls->setLayout(controlLayout);
	layout->addWidget(controls,1,2,Qt::AlignTop | Qt::AlignLeft);

	QLabel *algorithmLabel = new QLabel("algorithm");
	algorithmLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	controlLayout->addWidget(algorithmLabel,1,1);

	QComboBox *algorithmBox = new QComboBox();
	algorithmBox->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	QStringList algorithmList;
	algorithmList << QString(algorithmVariance.c_str()) << QString(algorithmExponential.c_str()) << QString(algorithmSimple.c_str());
	algorithmBox->addItems(algorithmList);
	controlLayout->addWidget(algorithmBox,1,2);

	ShadowmapSizePanel *smSizePanel = new ShadowmapSizePanel();
	smSizePanel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	controlLayout->addWidget(smSizePanel,2,1,1,2);

	QObject::connect(algorithmBox,SIGNAL(currentIndexChanged(int)),exampleListener,SLOT(chooseAlgorithm(int)));
	QObject::connect(smSizePanel,SIGNAL(emitSize(int,int,float)),exampleListener,SLOT(setShadowmapSize(int,int,float)));

	widget.show();

	return app.exec();
}

#endif
#ifndef _EXAMPLE_EXAMPLE_CPP_
#define _EXAMPLE_EXAMPLE_CPP_

#include <Example3.h>

ExampleListener::ExampleListener() {
}

void ExampleListener::create(SXRenderArea &area) {
	try {
		shadeX.addResources("../data/example3.c");
		shadeX.load();
	} catch(Exception &e) {
		Logger::get() << Level(L_ERROR) << e.getMessage();
		area.stopRendering();
		return;
	}
}

void ExampleListener::reshape(SXRenderArea &area) {
	RenderTarget &screen = shadeX.getRenderTarget("screen.target");
	screen.setWidth(area.getWidth());
	screen.setHeight(area.getHeight());
	screen.load();
}

void ExampleListener::render(SXRenderArea &area) {
	Pass &pass = shadeX.getPass("screen.pass");
	pass.render();
	
	float time = (float)area.getTime();
	Vector &objectData = shadeX.getUniformVector("triangle.translation");
	Matrix &passTransform = shadeX.getUniformMatrix("pass.rotation");
	objectData = Vector(0,(float)sin(time*Pi),0);
	passTransform = Matrix().rotate(Vector(0,0,1),(float)Pi * time);
}

void ExampleListener::stop(SXRenderArea &area) {
}

#endif
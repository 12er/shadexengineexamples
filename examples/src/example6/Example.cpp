#ifndef _EXAMPLE_EXAMPLE_CPP_
#define _EXAMPLE_EXAMPLE_CPP_

#include <Example6.h>

ExampleListener::ExampleListener() {
}

void ExampleListener::create(SXRenderArea &area) {
	try {
		shadeX.addResources("../data/example6.c");
		shadeX.load();
	} catch(Exception &e) {
		Logger::get() << Level(L_ERROR) << e.getMessage();
		area.stopRendering();
		return;
	}
}

void ExampleListener::reshape(SXRenderArea &area) {
	RenderTarget &screen = shadeX.getRenderTarget("screen.target");
	screen.setWidth(area.getWidth());
	screen.setHeight(area.getHeight());
	screen.load();
}

void ExampleListener::render(SXRenderArea &area) {
	Effect &effect = shadeX.getEffect("effect");
	effect.render();
	
	float time = (float)area.getTime();
	Matrix &fireflyTransform = shadeX.getUniformMatrix("firefly.transform");
	fireflyTransform = Matrix().rotate(Vector(0,0,1),(float)Pi * 0.2f * time) * Matrix().rotate(Vector(0,1,0),(float)Pi * time);
	Matrix &perspective = shadeX.getUniformMatrix("offline.projection");
	perspective.perspectiveMatrix((float)Pi*0.5f, (float)area.getWidth(), (float)area.getHeight(), 0.5f, 100.0f);
}

void ExampleListener::stop(SXRenderArea &area) {
}

#endif